<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>PHP Intro Lab</title>
  </head>
  <body class="container">
    <h1>PHP Intro Lab</h1>

    <form action="">
        <div class="form-group">
          <label for="word">Word</label>
          <input type="text" name="word" id="word" class="form-control" placeholder="enter word to hightlight">
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Highlight</button>
        </div>
    </form>
    
    <?php
        ini_set('display_errors',1);
        /********************************************
        * nl2br() Convert newline characters to the <br> tag. Every line break has a new line
        * character. They are there even if you not see them.
        *********************************************/
        $sonnet = nl2br("Shall I compare thee to a summer's day? 
            Thou art more lovely and more temperate:        
            Rough winds do shake the darling buds of May,
            And summer's lease hath all too short a date: 
            Sometime too hot the eye of heaven shines,
            And often is his gold complexion dimm'd;
            And every fair from fair sometime declines,
            By chance, or nature's changing course, untrimm'd;
            But thy eternal summer shall not fade
            Nor lose possession of that fair thou ow'st;
            Nor shall Death brag thou wander'st in his shade,
            When in eternal lines to time thou grow'st;
            So long as men can breathe or eyes can see,
            So long lives this, and this gives life to thee.");

        $wordCount = 0;
        
        $word = "";
        if ( isset( $_GET["word"]) ) {
            $word = trim( $_GET["word"]);
            $wordCount=substr_count($sonnet,$word); //
                                                    //
            /********************************************
            * substr_count() Returns the  the number of times substring occurs in the string
            *********************************************/
            
            $sonnet=str_replace($word,'<span style="background-color: #F9F902;">'.$word.'</span>',$sonnet);
            
            /////////////////
            // add your code here to count how many times $word appears
            // in $sonnet and also highlight each occurrence
            /////////////////
        }
    ?>
    <h2>
        <?php print ("'".$word."'") ?> appears <?php print $wordCount ?> times in this sonnet.
    </h2>
    <div>
        <?php
            // print the sonnet with highlighting
            print $sonnet;
        ?>
    </div>
    
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    
  </body>
</html>