<?php
    session_start();
    $message = "";
    include 'db.php';
    
    if ( ! isset ( $_SESSION["loggedin"] ) ) {
      $_SESSION["loggedin"] = false;
    }
    
    if ( isset ( $_REQUEST['inputEmail']) && isset ( $_REQUEST['inputPassword']) )  {
        $user   = addslashes($_REQUEST['inputEmail']);
        $pwd    = md5($_REQUEST['inputPassword']);
        $query  = selectDb("SELECT * FROM MEMBER WHERE EMAIL = '$user' and PASSWORD = '$pwd'");
        $member = mysqli_fetch_array($query);
            if(mysqli_num_rows($query) == 1){
            	$_SESSION['loggedin']   = true;
            	$_SESSION['user']       = $member['email'];
            	$_SESSION['userName']   = $member['Fname'].' '.$member['Lname'];
            	$_SESSION['MemberID']       = $member['MemberID'];
            	
                header( 'Location:index.php' );
            } else{
                $message = "Invalid email/password combination";
            }
    }
     
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

   <!--Bootstrap core CSS -->
      <link href="css/bootstrap.min.css" rel="stylesheet">
  
   <!-- Custom styles -->
  
    <link href="css/login.css" rel="stylesheet">

    <title>Login - Open Market</title>
  </head>


  <body class="text-center">
    <form class="form-signin" method="POST">
      <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
      <label for="inputEmail" class="sr-only">Email address</label>
      <input type="email" id="inputEmail" name="inputEmail" class="form-control" placeholder="Email address" required autofocus>
      <label for="inputPassword" class="sr-only">Password</label>
      <input type="password" id="inputPassword" name="inputPassword" class="form-control" placeholder="Password" required>
      <?php if ( ! empty( $message ) ) { ?>
        <div class="alert alert-danger" role="alert">
          <?php echo $message; ?>
        </div>  
      <?php } ?>
      <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      <p class="mt-5 mb-3 text-muted">&copy; 2019</p>
    </form>
  </body>
</html>