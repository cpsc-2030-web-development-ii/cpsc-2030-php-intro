<?php
    session_start();
    
    include 'db.php';
    
    if ( ! isset ( $_SESSION["loggedin"] ) ) {
      $_SESSION["loggedin"] = false;
    }
    if ($_SESSION["loggedin"]){
        $logFlag="Logout";
        $linkLogFlag="'logout.php'";
        $bidBtnFlag='btn btn-outline-success btn-block';
        $menuNavFlag="'nav-link'";
        $hiddenDelFlag="'btn btn-danger'";
        $userName=$_SESSION['userName'];
    } else{
        $logFlag="Log in";
        $linkLogFlag="'login.php'";
        $bidBtnFlag='btn btn-outline-success btn-block disabled';
        $menuNavFlag="'nav-link disabled'";
        $hiddenDelFlag="'d-none'";
        $userName='';
    }

    $loggedin = $_SESSION['loggedin'];
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="auctions sell buy offers">
  <meta name="author" content="Pablo Soares">

  <title>Open Market</title>

   <!--Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  
   <!--font awesome -->
	<link href="css/all.css" rel="stylesheet">

  <!-- Custom styles  -->
  <link href="css/style.css" rel="stylesheet">
  

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="index.php">Open Market</a>
      <span class="badge badge-dark"><?php !empty($userName) ? print "Hi, $userName"  : "" ; ?></span>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="index.php">Home
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="feedback.php">Feedbacks</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="documentation.php">Documentation</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="sources.php">Sources</a>
          </li>
          <li class="nav-item">
            <a class=<?php echo $menuNavFlag ?> href="account.php">Account</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href=<?php echo $linkLogFlag ?>><?php echo $logFlag ?></a>
          </li>

        </ul>
      </div>
    </div>
  </nav>
<!-- end nav --> 
<!-- fetching the lowest price in the database ----------------------------> 
	<?php
       	$results= selectDb('SELECT	itemID,
       								startBid AS minStartBid
       						FROM ITEM
       						WHERE startBid = (SELECT MIN(startBid) FROM ITEM)');

        	if ( ! $results ) {
              $error_number = mysqli_error( $link );
              $error_message = mysqli_error( $link );
              print "<tr><td colspan='6' class='alert alert-danger'>($error_number) $error_message</td></tr>";   
          } else {
              
              $record = mysqli_fetch_assoc( $results );
              $minBid = $record['minStartBid'];
              $minitemId = $record['itemID'];
              
  //<!-- header opening tag-------------------------------->
              echo "
  					<header>
					  <div class='container'>
						<div class='row'>
							<div class='col-lg-5 col-12 text-light jumbotron bg-transparent'>
					 			<a class='btn btn-success btn-mb' href='#btn$minitemId' role='button'>Starting at $$minBid</a>
              ";
              }
              mysqli_free_result( $results );
	?>
				
					<h1>Welcome to Open Market</h1>
					<h5 class="text-dark">Here you can sell and buy items for free, dealing directly with the sellers and buyers. No third party involved, no hidden costs, no surprise taxes.</h5>
					<a href="new_account.php" class="btn btn-dark btn-lg">Sign up for free</a>
				</div>
				<!--carrosel-->
				<div id="main-carousel" class="col-lg-7 col-12">
					<div id="img-home" class="carousel slide carousel-fade" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#img-home" data-slide-to="0" class="active"></li>
							<li data-target="#img-home" data-slide-to="1"></li>
							<li data-target="#img-home" data-slide-to="2"></li>
						</ol>
						<div class='carousel-inner'>
        
  <!--fetching latest 3 offers announced on to the database to insert in boostrap carousel control------------>
  <?php
       	$results= selectDb('SELECT * 
       						FROM ITEM 
       						ORDER BY startDt
       						DESC
       						LIMIT 3');        
            
        	if ( ! $results ) {
              $error_number = mysqli_error( $results );
              $error_message = mysqli_error( $results );
              print "<tr><td colspan='6' class='alert alert-danger'>($error_number) $error_message</td></tr>";   
          } else {
              $firstActiveItem=false;
              while( $record = mysqli_fetch_assoc( $results ) ) {
                $itemID = $record['itemID'];
                $itemTitle = $record['itemTitle'];
                $description = $record['description'];
                $PhotoPath = $record['PhotoPath'];
                $activeTag=(!$firstActiveItem ?' active' :'');

                # start of carrosel loop construction----------------------->	
  							# loading images of 3 last items added for sale ------------>
								echo "
									<div class='carousel-item $activeTag'>
										<a href='#btn$itemID'><img class='d-block w-100' height='300' alt='image of item $itemID' src='img/$itemID.jpg'></a>
										<div class='carousel-caption d-none d-md-block'>
											<h5 class='text-light'>$itemTitle</h5>
										  <p><a class='text-light' href='#item$itemID'>$description</a></p>
											<p><a class='text-light' href='$PhotoPath'> Copyright <i class='far fa-copyright'></i></a></p>
										</div>
									</div>
								";
							$firstActiveItem=true;
							
              }
              mysqli_free_result( $results );

            }
           
  ?>
								</div> <!--carousel inner close tag div -->
									<a class="carousel-control-prev" href="#img-home" role="button" data-slide="prev">
				            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
				            <span class="sr-only">Previous</span>
			            </a>
			            <a class="carousel-control-next" href="#img-home" role="button" data-slide="next">
				            <span class="carousel-control-next-icon" aria-hidden="true"></span>
				            <span class="sr-only">Next</span>
			            </a>
							</div>	<!-- carousel-slide close tag div -->
						</div>    <!-- main carousel close tag div -->
			</div>          <!--row close tag div -->
		</div>            <!--container close tag div-->
  </header>
	<!-- end of header -->
	<!-- starting cards construction----------------------------->
  <div id="course" class="container">
	<div class="row">
	
	<?php

		$results= selectDb('SELECT  i.itemID,
							        i.itemTitle,
							        i.description,
							        i.startDt,
							        i.endDt,
							        TIMESTAMPDIFF(DAY,startDt,endDt) as InitDeadline,
							        TIMESTAMPDIFF(DAY,NOW(),endDt) as DaysRemaining,
							        i.startBid,
							        i.bidIncrement,
							        i.PhotoPath,
							        i.MemberID,
							        b.TOTAL,
							        m.AVGR
							FROM    ITEM AS i
							LEFT JOIN (SELECT
							           BID.itemID,
							           COUNT(BID.BidID) AS TOTAL
									   FROM BID GROUP BY BID.itemID)
							           AS b ON i.itemID=b.itemID
							LEFT JOIN (SELECT
							           MemberID,
							           ROUND(AVG(Rate)) AS AVGR
							           FROM MEMBER_COMENTS_ON_WONTRANSACTION
							           GROUP BY MEMBER_COMENTS_ON_WONTRANSACTION.MemberID)
							           AS m ON i.MemberID=m.MemberID'); 

            
         if ( ! $results ) {
              $error_number = mysqli_error( $link );
              $error_message = mysqli_error( $link );
              print "<tr><td colspan='6' class='alert alert-danger'>($error_number) $error_message</td></tr>";   
          } else {
             # <!--- first loop---filling the main card tab------------------->
              while( $record = mysqli_fetch_assoc( $results ) ) {
                $itemID = $record['itemID'];
                $itemTitle = $record['itemTitle'];
                $InitDeadline= $record['InitDeadline'];
                $DaysRemaining= $record['DaysRemaining'];
                $startBid= $record['startBid'];
                $MemberID=$record['MemberID'];
                $description = $record['description'];
                $PhotoPath = $record['PhotoPath'];
                $totalBids=  $record['TOTAL'];		#<!-- the total of bids is shown in a badge-pill in the top of the tab--->
                $disabledFlag=(empty($totalBids)? ' disabled':''); #<!--- the tab of items without a bid is set to inactive--->
                $percentDays=100-round(($DaysRemaining*100)/$InitDeadline);
                echo "
				<div class='col-lg-3 col-md-6 col-12'>
					<div class='btn btn-info' id='btn$itemID'>$$startBid</div>
					<div class='card' id='card$itemID'>
					  <div class='embed-responsive embed-responsive-16by9'>
						<img src='img/$itemID.jpg' class='card-img-top embed-responsive-item' alt='image of item $itemID'>
					   </div>
		
						<div class='card-header'>
						  <ul class='nav nav-tabs card-header-tabs' id='tabitem$itemID' role='tablist'>
						    <li class='nav-item'>
						    	<a class='nav-link active' id='home-tab$itemID' data-toggle='tab' href='#item$itemID' role='tab' aria-controls='item$itemID'
						    	aria-selected='true'>Item</a>
						    </li>
						    <li class='nav-item'>
								<a class='nav-link  $disabledFlag' id='bid-tab$itemID' data-toggle='tab' href='#bid$itemID' role='tab'
								aria-controls='bid$itemID' aria-selected='false'>Bids<span class='badge badge-primary badge-pill'>$totalBids</span></a>
						    </li>
						    <li class='nav-item'>
						        <a class='nav-link' id='rate-tab$itemID' data-toggle='tab' href='#rate$itemID' role='tab'
						        aria-controls='rate$itemID' aria-selected='false'>Rates</a>
						    </li>
						  </ul>
						</div>					
						
						<div class='tab-content' id='tabcontent$itemID'>
							<!--<div class='card-body'> it is NOT necessary to use card-body when table-->
							<!--Main card tab-->
								<div class='tabtextcontent tab-pane fade show active' id='item$itemID' role='tabpanel' aria-labelledby='home-tab$itemID'>
									<h5 class='card-title'>
										$itemTitle
									</h5>
									<p style='font-size: 8px; color:#AAA'>
										 <a class='text-dark' href='$PhotoPath'>Photo Copyright <i class='far fa-copyright'></i> | </a>
										 <i class='fas fa-clock'></i> $DaysRemaining days remaining   
									</p>
									<div class='progress'>
									  <div class='progress-bar' role='progressbar' style='width: $percentDays%;' aria-valuenow='$percentDays'
									  aria-valuemin='0' aria-valuemax='100'>$percentDays%</div>
									</div>
									<p class='card-text'>$description</p>
									<a href='#$itemID' class='$bidBtnFlag'>Bid</a>
								</div>";
		            
		            
		            /*<!--Bid card tab-->*/
		            echo " 
		            <div class='tab-pane fade' role='tabpanel'  id='bid$itemID' aria-labelledby='bid-tab$itemID'>
						 <div class='container tablebidcontainer'>
							  <div class='row'>
								   <div class='col-md-12'>
									  <div class='scrollable'>
									    <table class='table table-hover table-striped table-fixed'>
									      <thead class='thead-dark'>
									        <tr>
									          <th class='text-center'>Date</th>
									          <th class='text-center'>Bid</th>
									        </tr>
									        </thead>";
									      
											$bidResults= selectDb("SELECT BTime, BPrice FROM BID WHERE itemID='$itemID' ORDER BY BPrice DESC");
										if ( ! $bidResults ) {
								              $error_number = mysqli_error( $link );
								              $error_message = mysqli_error( $link );
								              print "<tr><td colspan='6' class='alert alert-danger'>($error_number) $error_message</td></tr>";   
								          } else {
										      echo '<tbody>';
											
											while( $bidRecord = mysqli_fetch_assoc( $bidResults ) ) {
								                $BTime = $bidRecord['BTime'];
								                $BPrice = $bidRecord['BPrice'];
											         echo"<tr>
											        		<td>$BTime</td>
											        		<td class='text-right'>$BPrice</td>
					                    				 </tr>";
										  }      
								          	
								          
													    echo "</tbody>
																</table>
															   </div>
																 </div>
														 	  </div>
															</div>						 	  
															</div>
															<!--rate card tab-->
															<div class='tab-pane fade' role='tabpanel'  id='rate$itemID' aria-labelledby='rate$itemID'>";
							              					$rateResults= selectDb("SELECT ROUND(AVG(Rate)) AS AVGR FROM MEMBER_COMENTS_ON_WONTRANSACTION WHERE MemberID='$MemberID'");
							              					if ( ! $rateResults ) {
													              $error_number = mysqli_error( $link );
													              $error_message = mysqli_error( $link );
													              print "<tr><td colspan='6' class='alert alert-danger'>($error_number) $error_message</td></tr>";   
													        	} else {
													          		$rateRecord = mysqli_fetch_assoc( $rateResults );
															        $AVGR = $rateRecord['AVGR'];
															        if (!isset($AVGR)){
															        	$AVGR=0;
															        }
																
																echo "<h5 class='card-title'>";
																for ($i=1; $i <= 5; $i++){
																	if ($i <= $AVGR){
																	echo "<i class='fas fa-star text-warning'></i>";
																	#<!--light yellow stars accordingly the rate-->
																	} else{
																		echo "<i class='far fa-star'></i>";
																	}
																}
																
																echo "<br>
																			Average rate: $AVGR of 5.
																	  </h5>";
													        	}
										        				echo"	
																			</div>							
																	</div>
																</div>
															</div>";
											
											
											
											
											
										} /* end of the "else" statment of the second while loop */
            		} /* end of the first "while" loop */
          } /* end of the "else" statment of the first while loop */
?>



		</div>
		
	</div>
 <!-- Footer -->
  <footer class="py-5 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy; Open Market 2019</p>
    </div>
    <!-- /.container -->
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="jquery/jquery-3.4.1.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>

</body>

</html>