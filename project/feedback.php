<?php
    session_start();
    include 'db.php';
    
    if ( ! isset ( $_SESSION["loggedin"] ) ) {
      $_SESSION["loggedin"] = false;
    }
    
    if ($_SESSION["loggedin"]){
        $logFlag="Logout";
        $linkLogFlag="'logout.php'";
        $menuNavFlag="'nav-link'";
        $userName=$_SESSION['userName'];
    } else{
        $logFlag="Log in";
        $linkLogFlag="'login.php'";
        $menuNavFlag="'nav-link disabled'";
        $userName='';
    }

?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="auctions sell buy offers">
  <meta name="author" content="Pablo Soares">

  <title>Open Market</title>

   <!--Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  
  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet">
  

</head>

<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="index.php">Open Market</a>
      <span class="badge badge-dark"><?php !empty($userName) ? print "Hi, $userName"  : "" ; ?></span>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="index.php">Home
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="feedback.php">Feedbacks</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="documentation.php">Documentation</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="sources.php">Sources</a>
          </li>
          <li class="nav-item">
            <a class=<?php echo $menuNavFlag ?> href="account.php">Account</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href=<?php echo $linkLogFlag ?>><?php echo $logFlag ?></a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <header>
    <div class="container">
      
  <?php

		$results= selectDb('SELECT  i.MemberID,
							        i.Feedback,
							        n.Fname
							        
							FROM  MEMBER_COMENTS_ON_WONTRANSACTION AS i
							LEFT JOIN (SELECT
							           MEMBER.Fname,
							           MEMBER.MemberID
							           FROM MEMBER)
							           AS n ON i.MemberID = n.MemberID');

            
         if ( ! $results ) {
              $error_number = mysqli_error( $link );
              $error_message = mysqli_error( $link );
              print "<tr><td colspan='6' class='alert alert-danger'>($error_number) $error_message</td></tr>";   
          } else {
                  echo "
									    

									    <table class='table table-hover table-striped table-borderless'>
									      <thead class='thead-dark'>
									        <tr>
									          <th class='text-center'>Member ID</th>
									          <th class='text-center'>Name</th>
									          <th class='text-center'>Feedback</th>
									        </tr>
									        </thead>
									        <tbody>";
              
              while( $record = mysqli_fetch_assoc( $results ) ) {
                $iMemberID = $record['MemberID'];
				$iFeedback = $record['Feedback'];
				$nFname = $record['Fname'];
					
	                            echo "   <tr>
											    <td class='text-center'>$iMemberID</td>
											    <td>$nFname</td>
											    <td>$iFeedback</td>
					                    		
					                    	 </tr>";
              }
                                echo "</tbody>
                                  </table>
                                  ";
              
          }
?>
      
    </div>
  </header>
   <!-- Footer -->
   
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Open Market 2019</p>
      </div>
      <!-- /.container -->
    </footer>
   
  <!-- Bootstrap core JavaScript -->
  <script src="jquery/jquery-3.4.1.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>

  
  </body>
  
</html>