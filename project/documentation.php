<?php
    session_start();
    
    if ( ! isset ( $_SESSION["loggedin"] ) ) {
      $_SESSION["loggedin"] = false;
    }
    
    if ($_SESSION["loggedin"]){
        $logFlag="Logout";
        $linkLogFlag="'logout.php'";
        $menuNavFlag="'nav-link'";
        $userName=$_SESSION['userName'];
    } else{
        $logFlag="Log in";
        $linkLogFlag="'login.php'";
        $menuNavFlag="'nav-link disabled'";
        $userName='';
    }

?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="auctions sell buy offers">
  <meta name="author" content="Pablo Soares">

  <title>Open Market</title>

   <!--Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  
  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet">
  

</head>

<body>
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
          <div class="container">
            <a class="navbar-brand" href="index.php">Open Market</a>
            <span class="badge badge-dark"><?php !empty($userName) ? print "Hi, $userName"  : "" ; ?></span>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
              <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                  <a class="nav-link" href="index.php">Home
                    <span class="sr-only">(current)</span>
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="feedback.php">Feedbacks</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="documentation.php">Documentation</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="sources.php">Sources</a>
                </li>
                <li class="nav-item">
                  <a class=<?php echo $menuNavFlag ?> href="account.php">Account</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href=<?php echo $linkLogFlag ?>><?php echo $logFlag ?></a>
                </li>
              </ul>
            </div>
          </div>
      </nav>
<header>
  <div class="container">  
    
    <h1>DOCUMENTATION</h1>
    
    <h2>The general aspects</h2>
        <p>The utilization of the site is pretty straight forward.</p>
        <p>The site is designed to provide a place where sellers and buyers can freely negotiate.</p>
        <p>The carousel shows the last three products registered.</p>
        <p>In the loading procedure, all items are fetched from the database and showed with its price, bids, the ratting regarding the sellers, a description and photo.</p>
        <p>The cards show badges indicating the number of bids already received by an item.</p>
        <p>The cards and the web site are first mobile design provided with responsive bootstrap enhancements based on its grid system.</p>
        <p>If an item did not receive a bid, the tab is showed as inactive.</p>
        <p>It is necessary to have an account in order to be able to bid an item. Anyone can sign up for free.</p>
        <p>The following users were created for populate the database:</p>
        <img alt="image of user login accounts" src='img/Users.png'>
        <p>Their password are all the same and it is 'test'</p>
        <p>The password is encrypted with md5 PHP function, and all data are validated also on server-side before being inserted in the database as well as scapes are used to prevent possible SQL injection attacks.</p>
        <p> As security measures, the access to the database was implemented with an account with limited privileges instead of using the root password.</p>
        <p>Once logged, the buttons concerning the bid are activated. Unfortunately, I could not finish the bid procedure in time.</p>
        <p>The main page loading procedure was very challenging and I'm very proud to be accomplished in time.</p>
    <h2>The database</h2>
        <p>The database was also designed by me based upon what I've learned during CPSC-2221 course.</p>
        <p> It is derived from one of the course labs and was adapted to fit in this project</p>
        <p> I've mapping it by myself as well as the DDL commands on MySQL </p>
        <p>It is normalized and easy to maintain.</p>
      <h3> Here is the mapping result followed by the database diagram</h3>       
        <h4> Mapping</h4>
        <img alt="mapping process" src='img/DatabaseMapping.png'>
        <h4> Database diagram</h4>
        <img alt="Database diagram" src='img/DbDiagram.png'>
      <h3> The database was created in MySQL with the following DDL statments</h3>
          <p class="font-weight-bold">CREATE TABLE MEMBER(</p>
          <p>		MemberID int not null auto_increment,</p>
          <p>		Fname varchar(255) not null,</p>
          <p>		Lname varchar(255) not null,</p>
          <p>		email varchar(255) UNIQUE not null,</p>
          <p>		street varchar(255) not null,</p>
          <p>        city varchar (255) not null,</p>
          <p> 		province char(2) not null,</p>
          <p>        postCode char(6) not null,</p>
          <p>		password varchar(255) not null,</p>
          <p>		primary key (MemberID)</p>
          <p>	);</p>
          <p></p>
          <p class="font-weight-bold">CREATE TABLE ITEM(</p>
          <p>		itemID int not null auto_increment,</p>
          <p>		itemTitle varchar(255) not null,</p>
          <p>		descritption varchar(255) not null,</p>
          <p>		startDt date not null,</p>
          <p>		endDt date not null,</p>
          <p>		startBid numeric (5.2) not null,</p>
          <p>		bidIncrement numeric (5.2) not null,</p>
          <p>		PhotoPath varchar (255) not null,</p>
          <p>        MemberID int not null,</p>
          <p>        primary key (itemID),</p>
          <p>        foreign key (MemberID) references MEMBER(MemberID)</p>
          <p>	);</p>
          <p></p>
          <p></p>
          <p class="font-weight-bold">CREATE TABLE BID(</p>
          <p>		BidID int not null auto_increment,</p>
          <p>		BTime timestamp,</p>
          <p>		BPrice numeric (5.2) not null,</p>
          <p>		MemberID int,</p>
          <p>		itemID int,</p>
          <p>        primary key (BidID, MemberID, itemId),</p>
          <p>        foreign key (MemberID) references MEMBER(MemberID),</p>
          <p>        foreign key (itemID) references ITEM(itemID)</p>
          <p>	);</p>
          <p></p>
          <p></p>
          <p class="font-weight-bold">CREATE TABLE WON_TRANSACTION(</p>
          <p>		TransactID int not null auto_increment,</p>
          <p>		MemberID int,</p>
          <p>		itemID int,</p>
          <p>		BidID int,</p>
          <p>        primary key (TransactID),</p>
          <p>        foreign key (MemberID) references MEMBER(MemberID),</p>
          <p>        foreign key (itemID) references ITEM(itemID),</p>
          <p>        foreign key (BidID) references BID(BidID)</p>
          <p>	);</p>
          <p></p>
          <p></p>
          <p class="font-weight-bold">CREATE TABLE MEMBER_COMENTS_ON_WONTRANSACTION(</p>
          <p>		TransactID int,</p>
          <p>		MemberID int,</p>
          <p>		Feedback varchar(255),</p>
          <p>		Rate tinyint,</p>
          <p>        primary key (TransactID,MemberID),</p>
          <p>        foreign key (MemberID) references MEMBER(MemberID),</p>
          <p>        foreign key (TransactID) references WON_TRANSACTION(TransactID)</p>
          <p>	);</p>
    <h2> HTML validations screenshots</h2>
      <h3> Main page</h3>
        <img alt="Main page validation" src='img/MainpageValidation.png'>
      <h3> Account page</h3>
        <img alt="Account page validation" src='img/AccountpageValidation.png'>
      <h3> New account page</h3>
        <img alt="New account page validation" src='img/NewAccountpageValidation.png'>
      <h3> Document page</h3>
        <img alt="Document page validation" src='img/DocumentpageValidation.png'>
      <h3> Feedback page</h3>
        <img alt="Feedback page validation" src='img/FeedbackpageValidation.png'>
      <h3> Sources page</h3>
        <img alt="Sources page validation" src='img/SourcepageValidation.png'>
    <h2> CSS validations screenshots</h2>
      <h3> Style</h3>
        <img alt="Style CSS validation" src='img/StyleCssValidation.png'>
      <h3> Login </h3>
        <img alt="Login CSS validation" src='img/LoginCssValidation.png'>
    
</div>
</header>

 <!-- Footer -->
  <footer class="py-5 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy; Open Market 2019</p>
    </div>
    <!-- /.container -->
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="jquery/jquery-3.4.1.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>

</body>

</html>