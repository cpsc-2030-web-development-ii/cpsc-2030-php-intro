<?php
    session_start();
    
    if ( ! isset ( $_SESSION["loggedin"] ) ) {
      $_SESSION["loggedin"] = false;
    }
    
    if ($_SESSION["loggedin"]){
        $logFlag="Logout";
        $linkLogFlag="'logout.php'";
        $menuNavFlag="'nav-link'";
        $userName=$_SESSION['userName'];
    } else{
        $logFlag="Log in";
        $linkLogFlag="'login.php'";
        $menuNavFlag="'nav-link disabled'";
        $userName='';
    }

?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="auctions sell buy offers">
  <meta name="author" content="Pablo Soares">

  <title>Open Market</title>

   <!--Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  
  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet">
  

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="index.php">Open Market</a>
      <span class="badge badge-dark"><?php !empty($userName) ? print "Hi, $userName"  : "" ; ?></span>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="index.php">Home
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="feedback.php">Feedbacks</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="documentation.php">Documentation</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="sources.php">Sources</a>
          </li>
          <li class="nav-item">
            <a class=<?php echo $menuNavFlag ?> href="account.php">Account</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href=<?php echo $linkLogFlag ?>><?php echo $logFlag ?></a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <header>
  <div class="container">  
      <form id="userAccount" class="needs-validation" method="POST" action="add_account.php" novalidate>
        <div class="form-row">
          <div class="col-md-4 mb-3">
            <label for="validationCustom01">First name</label>
            <input type="text" class="form-control" name="Fname" id="validationCustom01" placeholder="First name" value='<?php echo $Fname ?>' required>
            <div class="valid-feedback">
              Looks good!
            </div>
          </div>
          <div class="col-md-4 mb-3">
            <label for="validationCustom02">Last name</label>
            <input type="text" class="form-control" name="Lname" id="validationCustom02" placeholder="Last name" value='<?php echo $Lname ?>' required>
            <div class="valid-feedback">
              Looks good!
            </div>
          </div>
          <div class="col-md-4 mb-3">
            <label for="validationCustomUsername">Email</label>
            <div class="input-group">
              <div class="input-group-pospend">
                <span class="input-group-text" id="inputGroupPospend">@</span>
              </div>
              <input type="email" class="form-control" name="email" id="validationCustomUsername" placeholder="Enter email" aria-describedby="validationCustomUsername" value='<?php echo $email ?>' required>
              <div class="invalid-feedback">
                Please enter a valid email.
              </div>
            </div>
          </div>
        </div>
        <div class="form-row">
          <div class="col-md-6 mb-3">
            <label for="validationCustom06">Street</label>
            <input type="text" class="form-control" name="street" id="validationCustom06" placeholder="Street"  value='<?php echo $street ?>' required>
            <div class="invalid-feedback">
              Please provide a valid street.
            </div>
          </div>
          
          <div class="col-md-6 mb-3">
            <label for="validationCustom03">City</label>
            <input type="text" class="form-control" name="city" id="validationCustom03" placeholder="City"  value='<?php echo $city ?>' required>
            <div class="invalid-feedback">
              Please provide a valid city.
            </div>
          </div>
          <div class="col-md-3 mb-3">
            <label for="validationCustom04">Province</label>
            <input type="text" class="form-control" name="province" id="validationCustom04" placeholder="Province"  value='<?php echo $province ?>' required>
            <div class="invalid-feedback">
              Please provide a valid state.
            </div>
          </div>
          <div class="col-md-3 mb-3">
            <label for="validationCustom05">Postcode</label>
            <input type="text" class="form-control" name="postCode" id="validationCustom05" placeholder="Post Code"  value='<?php echo $postCode ?>' required>
            <div class="invalid-feedback">
              Please provide a valid zip.
            </div>
          </div>
          <div class="col-md-3 mb-3">
            <label for="validationCustom07">Password</label>
            <input type="password" class="form-control" name="password" id="validationCustom07" value='' required>
            <div class="invalid-feedback">
              Please provide a valid password.
            </div>
          </div>
    
        </div>
        <button class="btn btn-primary btn-block" type="submit">Sing up</button>
      </form>
      
      <script>
      // Example starter JavaScript for disabling form submissions if there are invalid fields
      (function() {
        'use strict';
        window.addEventListener('load', function() {
          // Fetch all the forms we want to apply custom Bootstrap validation styles to
          var forms = document.getElementsByClassName('needs-validation');
          // Loop over them and prevent submission
          var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
              if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
              }
              //console.log(validation);
              form.classList.add('was-validated');
            }, false);
          });
        }, false);
      })();
      </script>
  </div>
</header>
   <!-- Footer -->
  <footer class="py-5 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy; Open Market 2019</p>
    </div>
    <!-- /.container -->
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="jquery/jquery-3.4.1.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>

</body>
</html>