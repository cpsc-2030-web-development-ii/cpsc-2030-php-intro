<?php
    session_start();
          $_SESSION["loggedin"] = false;
          //header('Location: index.php'); <!--- it is not necessary a header due
                                          // AJAX request
          
    $logout=!($_SESSION["loggedin"]);
    // It was necessary to use the NOT "!" operator in order to assure a true return
    // in case of successful logout process
    $logStatus->logout =$logout;
    $jSONlogStatus=json_encode($logStatus);
    echo $jSONlogStatus;
?>