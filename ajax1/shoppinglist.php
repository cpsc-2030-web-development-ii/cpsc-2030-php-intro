<?php
    session_start();
    // It is necessary to start the sesion also in the page
    // called by the header() function in order to be able to
    // access the value writen in the super global $_SESSION
    // associative array by the previous page 
?>

<?php
    header( "Content-type: application/json");
    
    $link = mysqli_connect( 'localhost', 'root', '9C6#,WUQ' );
    if ( ! $link ) {
      $error_number = mysqli_connect_errno();
      $error_message = mysqli_connect_error();
      file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
      http_response_code( 500 );
      exit(1);
    }
    $dbName = "DataCars";
    if ( ! mysqli_select_db( $link, $dbName ) ) {
      $error_number = mysqli_errno( $link );
      $error_message = mysqli_error( $link );
      file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
      http_response_code( 500 );
      exit(1);
    }
    
    switch ( $_SERVER['REQUEST_METHOD']) {
        case 'GET':
            $results = mysqli_query( $link, 'SELECT * FROM cars' );
            
            if ( ! $results ) {
                $error_number = mysqli_error( $link );
                $error_message = mysqli_error( $link );
                file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
                http_response_code( 500 );
                exit(1);
            } else {
                $shoppinglist = array();
                while( $record = mysqli_fetch_assoc( $results ) ) {
                    $shoppinglist[] = $record;
                }
                mysqli_free_result( $results );
                echo json_encode( $shoppinglist );
            }
            break;
        
        case 'POST':
            $safe_description = mysqli_real_escape_string( $link, $_REQUEST["Make"] );
            $quantity = $_REQUEST["Mileage"] + 0;
            $make= $_REQUEST["Make"];
            $model= $_REQUEST["Model"];
            $mileage= $_REQUEST["Mileage"];
            $year= $_REQUEST["Year"];
            
            if ( strlen( $safe_description ) <= 0 ||
                 strlen( $safe_description ) > 80 ||
                 $quantity <= 0 ) {
                file_put_contents( "/tmp/ajax.log", "Invalid Client Request\n", FILE_APPEND );
                http_response_code( 400 );
                exit(1);
            }

            $query = "INSERT INTO cars (Make, Model, Mileage, Year) VALUES ('$make', '$model', $mileage, $year)";
            
            // $query = "INSERT INTO CARS ( Quantity, Description ) VALUES ( $quantity, '$safe_description' )";
            if ( ! mysqli_query( $link, $query ) ) {
              $error_number = mysqli_errno( $link );
              $error_message = mysqli_error( $link );
              file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
              http_response_code( 500 );
            } else{
                //another option in MySQLi Procedural: $lastRowId=mysqli_insert_id($link);
                
                $lastRowID=$link->insert_id;
                $RowIdObj->ID = $lastRowID;
                $jSONId=json_encode($RowIdObj);
                echo $jSONId;
                //return statment do not works for this purpose $lastRowID;
            }
            break;
        
        case 'DELETE':
            //$ID = $_REQUEST['ID'];
            //
            // your code to complete the delete operation goes here
            //
            $queries = array();
            parse_str($_SERVER['QUERY_STRING'], $queries);
            $rowID=$queries['ID'];
            $query = "DELETE FROM cars WHERE ID='$rowID'";
            
            // $query = "INSERT INTO CARS ( Quantity, Description ) VALUES ( $quantity, '$safe_description' )";
            if ( ! mysqli_query( $link, $query ) ) {
              $error_number = mysqli_errno( $link );
              $error_message = mysqli_error( $link );
              file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
              http_response_code( 500 );
              
            } else{
            
            $RowIdObj->Deleted = true;
            $RowIdObj->ID = $queries['ID'];
            $jSONId=json_encode($RowIdObj);
            echo $jSONId;
            }
            break;

    }
