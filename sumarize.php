<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title> CPSC 2030</title>

    <!-- Bootstrap core CSS -->

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }
      
      .inputfields{
        width:60px;
        height:30px;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    
  </head>
  <body class="bg-light">
    <div class="container">                              <!-- CONTAINER -->
  <div class="py-5 text-center">
    <img class="d-block mx-auto mb-4" src="/docs/4.3/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72">
    <h2>Order Summary</h2>
  </div>
    <div class="row">                                                               <!-- ROW -->
     <div class="col-md-4 order-md-2 mb-4">
  
      <?php
        $provinceTax = array("Alberta"=>0.05,
                            "British Columbia"=>0.12,
                            "Manitoba"=>0.13,
                            "New-Brunswick"=>0.15,
                            "Newfoundland and Labrador"=>0.15,
                            "Northwest Territories"=>0.05,
                            "Nova Scotia"=>0.15,
                            "Nunavut"=>0.05,
                            "Ontario"=>0.13,
                            "Prince Edward Island"=>0.15,
                            "Québec"=>0.14975,
                            "Saskatchewan"=>0.11,
                            "Yukon"=>0.05);

    
        $firstName=$_POST["firstName"];
        $lastName=$_POST["lastName"];
        $email=$_POST["email"];
        $address=$_POST["address"];
        $address2=$_POST["address2"];
        $country=$_POST["country"];
        $province=$_POST["province"];
        $poltalcode=$_POST["postalcode"];
        $giftwrap=$_POST["giftwrap"];
        $cc_name=$_POST["cc-name"];
        $cc_number=$_POST["cc-number"];
        $cc_expiration=$_POST["cc-expiration"];
        $cc_cvv=$_POST["cc-cvv"];
        $firstQtd=$_POST["firstQtd"];
        $secondQtd=$_POST["secondQtd"];
        $thirdQtd=$_POST["thridQtd"];
        $deliveryMethod=$_POST["deliverymethod"];
        
        $totalBeforeTax=(12*$firstQtd)+(8*$secondQtd)+(5*$thirdQtd)+$deliveryMethod;
        
        if ($_POST["giftwrap"]){
          $totalBeforeTax+=6.99;
        }
        
        $taxPercentage=number_format(($provinceTax[$province]*100),2);
        $taxAmount=$totalBeforeTax*$provinceTax[$province];
        $totalFinal=$totalBeforeTax+$taxAmount;
        $totalBeforeTax=number_format($totalBeforeTax,2);
        $taxAmount=number_format($taxAmount,2);
        $totalFinal=number_format($totalFinal,2);

        print "
              <h4 class=\"d-flex justify-content-between align-items-center mb-3\">
                <span class=\"text-muted\">Your order</span>
                <span class=\"badge badge-secondary badge-pill\">3</span>
              </h4>
              <ul class=\"list-group mb-3\">                                               
                <li class=\"list-group-item d-flex justify-content-between lh-condensed\">
                  <div>
                    <h6 class=\"my-0\">Product name</h6>
                    <small class=\"text-muted\">Brief description</small>
                  </div>
                  <span class=\"text-muted\">$12</span>
                  <span class=\"text-muted\">$firstQtd</span>
                </li>
                <li class=\"list-group-item d-flex justify-content-between lh-condensed\">
                  <div>
                    <h6 class=\"my-0\">Second product</h6>
                    <small class=\"text-muted\">Brief description</small>
                  </div>
                  <span class=\"text-muted\">$8</span>
                  <span class=\"text-muted\">$secondQtd</span>
                </li>
                <li class=\"list-group-item d-flex justify-content-between lh-condensed\">
                  <div>
                    <h6 class=\"my-0\">Third item</h6>
                    <small class=\"text-muted\">Brief description</small>
                  </div>
                  <span class=\"text-muted\">$5</span>
                  <span class=\"text-muted\">$thirdQtd</span>
                </li>
              ";

        if ($_POST["giftwrap"]){
          print "
                <li class=\"list-group-item d-flex justify-content-between lh-condensed\">
                  <div>
                    <h6 class=\"my-0\">Gift Wrap</h6>
                    <small class=\"text-muted\">Brief description</small>
                  </div>
                  <span class=\"text-muted\">$6.99</span>
                  <span class=\"text-muted\">1</span>
                </li>

              ";
        };

        print "
                <li class=\"list-group-item d-flex justify-content-between lh-condensed\">
                  <div>
                    <h6 class=\"my-0\">Delivery</h6>
                    <small class=\"text-muted\">Brief description</small>
                  </div>
                  <span class=\"text-muted\">$$deliveryMethod</span>
                  <span class=\"text-muted\"></span>
                </li>
                <li class=\"list-group-item d-flex justify-content-between lh-condensed\">
                  <div>
                    <h6 class=\"my-0\">Total before tax</h6>
                    <small class=\"text-muted\">Brief description</small>
                  </div>
                  <span class=\"text-muted\">$$totalBeforeTax</span>
                  <span class=\"text-muted\"></span>
                </li>
                <li class=\"list-group-item d-flex justify-content-between lh-condensed\">
                  <div>
                    <h6 class=\"my-0\">Tax $taxPercentage % </h6>
                    <small class=\"text-muted\">$province</small>
                  </div>
                  <span class=\"text-muted\">$$taxAmount</span>
                  <span class=\"text-muted\"></span>
                </li>
                <li class=\"list-group-item d-flex justify-content-between lh-condensed\">
                  <div>
                    <h6 class=\"my-0 text-success\">Total (CAD)</h6>
                  </div>
                  <strong class=\"text-success\">$$totalFinal</strong>
                  <span class=\"text-muted\"></span>
                </li>
              </ul>
            </div>

              ";

        print "
              <div class=\"col-md-8 order-md-1\">
                <h4 class=\"mb-3\">Billing address</h4>
                  <div class=\"row\">
                    <div class=\"col-md-6 mb-3\">
                      <h5>First name</h5>
                      <h6> $firstName </h6>
                    </div>
                    <div class=\"col-md-6 mb-3\">
                      <h5>Last name</h5>
                      <h6> $lastName <h6>
                    </div>
              </div>";
              
        if (!empty($email)) {
          print "
                <h5 class=\"mb-3\">Email</h5>
                  <h6> $email </h6>
                <hr class=\"mb-4\">
                ";
        }
        
        print "
              <h5 class=\"mb-3\">Address</h5>
                <h6> $address </h6>
                    ";
              
        if (!empty($address2)) {
          print "
                <h5 class=\"mb-3\">Address 2</h5>
                  <h6> $address2 </h6>
                      ";
        }
        
        print "
              <h5 class=\"mb-3\">Country</h5>
                <h6> $country </h6>
              <h5 class=\"mb-3\">Province</h5>
                <h6> $province </h6>
              <h5 class=\"mb-3\">Postal Code</h5>
                <h6> $poltalcode </h6>
              <hr class=\"mb-4\">
              <h5 class=\"mb-3\">Name on Card</h5>
                <h6> $cc_name </h6>
              <h5 class=\"mb-3\">Credit Card Number</h5>
                <h6> $cc_number </h6>
              <h5 class=\"mb-3\">Expiration</h5>
                <h6> $cc_expiration </h6>
              <h5 class=\"mb-3\">CVV</h5>
                <h6> $cc_cvv </h6>
                    ";
       
          
        if ($totalFinal>750){
          print "
          <div class=\"alert alert-danger\" role=\"alert\">
            Your credit card has been declined
          </div> 
        
          ";
        }       
        
        
        // print "Delivery method: ". $_POST["deliverymethod"]. "<br>";
        // print "The total is: ".(12*$_POST["firstQtd"]+8*$_POST["secondQtd"]+5*$_POST["thridQtd"]). "<br>";
        // print "The tax for $province is : ".(number_format(($provinceTax[$province]*100),2))." % <br>";
        // print "Total before taxes: $totalBeforeTax <br>";
        // print "Taxes amount: $taxAmount <br>";
        // print "Total: $totalFinal <br>";

      ?>

    <!--    </tbody>-->
    <!--</table>-->
    
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
   </div>  
 </div>
      <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
      <script>window.jQuery || document.write('<script src="/docs/4.3/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
      <script src="/docs/4.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script>

  </body>
</html>