<?php
    session_start();
    // It is necessary to start the sesscion also in the page
    // called by the header() function in order to be able to
    // access the value writen in the super global $_SESSION
    // associative array by the previous page 
    if ($_SESSION["loggedin"]){
        $textButton="Logout";
        $linkButton="'logout.php'";
        $hiddenAddBtnFlag="'btn btn-primary btn-lg btn-block'";
        $hiddenDelFlag="'btn btn-danger'";
        
    } else{
        $textButton="Login";
        $linkButton="'login.php'";
        $hiddenAddBtnFlag="'d-none btn btn-primary btn-lg btn-block'";
        $hiddenDelFlag="'d-none'";
    
    }
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Cars - CPSC 2030</title>
  </head>
  <nav>
      <ul class="nav nav-pills">
         <li class="nav-item">
           <a class="nav-link active" href=<?php print $linkButton ?>><?php print $textButton ?> </a>
        </li>
      </ul>
  </nav>
  <body class="container">
    <h1>Cars - CPSC 2030</h1>

    <form class="was-validated" novalidate method="POST" action="">
        <div class="row">
          <div class="col-md-6 mb-3">
            <label for="make">Make</label>
            <input type="text" class="form-control" id="make" placeholder="" value="" required name="make">
            <div class="valid-feedback">
                Valid.
            </div>
            <div class="invalid-feedback">
                Valid make is required.
            </div>
          </div>
          <div class="col-md-6 mb-3">
            <label for="model">Model</label>
            <input type="text" class="form-control" id="model" placeholder="" value="" required name="model">
            <div class="valid-feedback">
                Valid.
            </div>
            <div class="invalid-feedback">
                Valid model is required.
            </div>
          </div>
          <div class="col-md-6 mb-3">
            <label for="model">Mileage</label>
            <input type="number" class="form-control" id="mileage" placeholder="" value="" required name="mileage">
            <div class="valid-feedback">
                Valid.
            </div>
            <div class="invalid-feedback">
                Valid mileage is required.
            </div>
          </div>
          <div class="col-md-6 mb-3">
            <label for="model">Year</label>
            <input type="number" class="form-control" id="year" placeholder="" value="" required name="year">
            <div class="valid-feedback">
                Valid.
            </div>
            <div class="invalid-feedback">
                Valid year is required.
            </div>
            <input type="hidden" id="operation" name="operation" value="add">
          </div>
        </div>
        <button class=<?php print $hiddenAddBtnFlag ?> type="submit">Add car</button>
    </form>

    <table class="table">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Make</th>
                <th scope="col">Model</th>
                <th scope="col"> Mileage</th>
                <th scope="col"> Year </th>
            </tr>
        </thead>
        <tbody>
           
           <?php
        //   print "Logged in sucessfull? ". $_SESSION["loggedin"];
           $link= mysqli_connect( 'localhost', 'root', '9C6#,WUQ');
           
           if (!$link) {
             print "<div class='alert alert-danger' role='alert'>
                    Error: Unable to connect to MySQL." . PHP_EOL."<br>
                    Debugging errno: " . mysqli_connect_errno() . PHP_EOL."<br>
                    Debugging error: " . mysqli_connect_error() . PHP_EOL.
                    "</div>";
            } else {
                $mysqli=mysqli_select_db ($link,'DataCars');
                if (!$mysqli){
                    printf("Connect failed: %s\n", mysqli_connect_error());
                    
                } else{
                       $make=$_POST["make"];
                       $model=$_POST["model"];
                       $mileage=$_POST["mileage"];
                       $year=$_POST["year"];
                       $operation=$_POST["operation"];
                                
                  if ($operation=="add"){
                      if (!preg_match("/^./", $make)){
                          print "<div class='alert alert-danger' role='alert'>
                                ERROR: make must be filled with at least one
                                character! </div>";
                          
                      }else if (!preg_match("/^./", $model)){
                          print "<div class='alert alert-danger' role='alert'>
                                ERROR: model must be filled with at least one
                                character! </div>";
                            
                            } else if (!preg_match("/^[0-9]+$/", $mileage)){
                              print "<div class='alert alert-danger' role='alert'>
                                    ERROR: mileage must be filled with numeric
                                    digits! </div>";
                               
                               } else if (!preg_match("/^[0-9][0-9][0-9][0-9]$/", $year)){
                                      print "<div class='alert alert-danger' role='alert'>
                                            ERROR: year must be filled with FOUR numeric
                                            digits! </div>";
                                    
                                }else{
                                      $mysqliqry=mysqli_query($link,"INSERT INTO cars (Make, Model, Mileage, Year)
                                      VALUES ('$make', '$model', $mileage, $year)");
                                      if (!$mysqliqry){
                                          print "<div class='alert alert-danger' role='alert'>
                                                ERRO:Connect failed:".mysqli_connect_error(). 
                                                "</div>";
                                      }
                                }
                                             

                  }else if ($operation=="delete"){
                      $rowID=$_POST["DeleteBtn"];
                    //   echo "<script type='text/javascript'>alert('$rowID');</script>";
                      $mysqliqry=mysqli_query($link,"DELETE FROM cars WHERE ID='$rowID'");
                      if (!$mysqliqry){
                          print "<div class='alert alert-danger' role='alert'>
                                ERROR:Connect failed:".mysqli_connect_error().
                                "</div>";
                      }
                     }
                     
                     $results = mysqli_query($link, 'SELECT * FROM cars');
                      if (!$results){
                          print "<div class='alert alert-danger' role='alert'>
                                ERROR:Connect failed:".mysqli_connect_error().
                                "</div>";
                      } else{
                          
                             // process $results
                            while ($record = mysqli_fetch_assoc($results)){
                             $ID = $record['ID'];
                             $make= $record ['Make'];
                             $model= $record ['Model'];
                             $mileage=$record ['Mileage'];
                             $year=$record['Year'];
                             print "<tr><td> $ID</td><td>$make</td><td>$model</td>
                             <td>$mileage</td><td>$year</td>
                             <td><form method='POST' action=''> <button class= ".
                             $hiddenDelFlag." type='submit' name='DeleteBtn' value='$ID'>
                             Delete</button> <input type='hidden' name='operation'
                             value='delete'> <input type='hidden' name='id'
                             value='$ID'></td></tr>"
                             ;
                            }
                      }
                 }
            }
           

           mysqli_free_result($results);
           mysqli_close($link);

           ?> 


        </tbody>
    </table>
    
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    
   
  </body>
</html>