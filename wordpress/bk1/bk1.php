<?php
/**
 * Plugin Name:	BK One
 * Version: 		0.1.0
 * Author:  		Brian Koehler
 * Description: 	This plugin is a demonstration for CPSC 2030.
 * Author URI:  	https://langarabrian.gitlab.io
 * Plugin URI:  	https://brianbrian.chickenkiller.com/wordpres
 */

function bk1_title_extender( $title ) {
    $options = get_option( 'bk1_options' );
    $suffix = $options['bk1_field_suffix'];
    $prefix = $options['bk1_field_prefix'];
    return $prefix . $title . $suffix;
}
  
add_filter( 'the_title', 'bk1_title_extender' );

function bk1_footer_text() {
    echo "<p>This is the end!</p>";
}
  
add_action( 'wp_footer', 'bk1_footer_text' );

/*******************************************************************/

/**
 * @internal never define functions inside callbacks.
 * these functions could be run multiple times; this would result in a fatal error.
 */
 
/**
 * custom option and settings
 */
function bk1_settings_init() {
 // register a new setting for "bk1" page
 register_setting( 'bk1', 'bk1_options' );
 
 // register a new section in the "bk1" page
 add_settings_section(
 'default',
 __( 'Title Settings', 'bk1' ),
 'bk1_section_developers_cb',
 'bk1'
 );
 
 add_settings_field(
 'bk1_field_prefix',
 
 __( 'Title Prefix', 'bk1' ),
 'bk1_field_prefix_cb',
 'bk1',

 'default',
 [
 'label_for' => 'bk1_field_prefix',
 'class' => 'bk1_row',
 'bk1_custom_data' => 'custom',
 ]
 );
 
 
 // register a new field in the "bk1_section_developers" section, inside the "bk1" page
 add_settings_field(
 'bk1_field_suffix',
 // as of WP 4.6 this value is used only internally
 // use $args' label_for to populate the id inside the callback
 __( 'Title Suffix', 'bk1' ),
 'bk1_field_suffix_cb',
 'bk1',
// 'bk1_section_developers',
 'default',
 [
 'label_for' => 'bk1_field_suffix',
 'class' => 'bk1_row',
 'bk1_custom_data' => 'custom',
 ]
 );

}
 
 
/**
 * register our bk1_settings_init to the admin_init action hook
 */
add_action( 'admin_init', 'bk1_settings_init' );
 
/**
 * custom option and settings:
 * callback functions
 */
 
// developers section cb
 
// section callbacks can accept an $args parameter, which is an array.
// $args have the following keys defined: title, id, callback.
// the values are defined at the add_settings_section() function.
function bk1_section_developers_cb( $args ) {
}
 
// suffix field cb
 
// field callbacks can accept an $args parameter, which is an array.
// $args is defined at the add_settings_field() function.
// wordpress has magic interaction with the following keys: label_for, class.
// the "label_for" key value is used for the "for" attribute of the <label>.
// the "class" key value is used for the "class" attribute of the <tr> containing the field.
// you can add custom key value pairs to be used inside your callbacks.


function bk1_field_prefix_cb( $args ) {
 // get the value of the setting we've registered with register_setting()
 $options = get_option( 'bk1_options' );
 // output the field
 ?>
 <input type="text" id="<?php echo esc_attr( $args['label_for'] ); ?>"
    name="bk1_options[<?php echo esc_attr( $args['label_for'] ); ?>]"
    value="<?php echo isset( $options[ $args['label_for'] ] ) ? ($options[ $args['label_for'] ]) : ('') ?>">
 <?php
}

function bk1_field_suffix_cb( $args ) {
 // get the value of the setting we've registered with register_setting()
 $options = get_option( 'bk1_options' );
 // output the field
 ?>
 <input type="text" id="<?php echo esc_attr( $args['label_for'] ); ?>"
    name="bk1_options[<?php echo esc_attr( $args['label_for'] ); ?>]"
    value="<?php echo isset( $options[ $args['label_for'] ] ) ? ($options[ $args['label_for'] ]) : ('') ?>">
 <?php
}
 
 
/**
 * top level menu
 */
function bk1_options_page() {
 // add top level menu page
 add_menu_page(
 'BK1',
 'BK1 Options',
 'manage_options',
 'bk1',
 'bk1_options_page_html'
 );
}
 
/**
 * register our bk1_options_page to the admin_menu action hook
 */
add_action( 'admin_menu', 'bk1_options_page' );
 
/**
 * top level menu:
 * callback functions
 */
function bk1_options_page_html() {
 // check user capabilities
 if ( ! current_user_can( 'manage_options' ) ) {
 return;
 }
 
 // add error/update messages
 
 // check if the user have submitted the settings
 // wordpress will add the "settings-updated" $_GET parameter to the url
 if ( isset( $_GET['settings-updated'] ) ) {
 // add settings saved message with the class of "updated"
 add_settings_error( 'bk1_messages', 'bk1_message', __( 'Settings Saved', 'bk1' ), 'updated' );
 }
 
 // show error/update messages
 settings_errors( 'bk1_messages' );
 ?>
 <div class="wrap">
 <h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
 <form action="options.php" method="post">
 <?php
 // output security fields for the registered setting "bk1"
 settings_fields( 'bk1' );
 // output setting sections and their fields
 // (sections are registered for "bk1", each field is registered to a specific section)
 do_settings_sections( 'bk1' );
 // output save settings button
 submit_button( 'Save Settings' );
 ?>
 </form>
 </div>
 <?php
}

/**
 * Adds Foo_Widget widget.
 */
class Foo_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'foo_widget', // Base ID
			esc_html__( 'Widget Title', 'text_domain' ), // Name
			array( 'description' => esc_html__( 'A Foo Widget', 'text_domain' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}
		echo esc_html__( 'Hello, World!', 'text_domain' );
		echo $args['after_widget'];
		
		if (! empty( $instance['User_count'] )){
	    $resultUsers = count_users();
     echo 'There are ', $resultUsers['total_users'], ' total users';
     foreach( $resultUsers['avail_roles'] as $role => $count )
          echo ', ', $count, ' are ', $role, 's';
      echo '.';
      echo "<br>";
		}

		if (! empty( $instance['Post_count'] )){
		    $count_posts = wp_count_posts()->publish;
		   
		    echo 'Total posts are '.$count_posts;
		} 
	
	}

	/**
	 * Back-end widget form.  
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( 'New title', 'text_domain' );
		$Post_count = ! empty( $instance['Post_count'] ) ? 'checked' : esc_html__( '' );
		$User_count = ! empty( $instance['User_count'] ) ? 'checked' : esc_html__( '' );
		?>
		<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_attr_e( 'Title:', 'text_domain' ); ?></label> 
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>

		<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'Post_count' ) ); ?>"><?php esc_attr_e( 'Post count:', 'text_domain' ); ?></label> 
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'Post_count' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'Post_count' ) ); ?>" type="checkbox" value="Post_count" <?php echo esc_attr( $Post_count ); ?>>
		</p>

		<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'User_count' ) ); ?>"><?php esc_attr_e( 'User count:', 'text_domain' ); ?></label> 
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'User_count' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'User_count' ) ); ?>" type="checkbox" value="User_count" <?php echo esc_attr( $User_count ); ?>>
		</p>
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? sanitize_text_field( $new_instance['title'] ) : '';
		$instance['User_count'] = ( ! empty( $new_instance['User_count'] ) ) ? sanitize_text_field( $new_instance['User_count'] ) : '';
		$instance['Post_count'] = ( ! empty( $new_instance['Post_count'] ) ) ? sanitize_text_field( $new_instance['Post_count'] ) : '';

		return $instance;
	}

} // class Foo_Widget

// register Foo_Widget widget
function register_foo_widget() {
    register_widget( 'Foo_Widget' );
}
add_action( 'widgets_init', 'register_foo_widget' );