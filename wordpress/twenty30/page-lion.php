<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
?>

<style>
 /*.wp-block-quote, h2 {*/
 /*    background-color: #FFDEAD;*/
 /*}*/
 /*.site-content-contain {*/
 /*    background-color: #F5DEB3;*/
 /*    font-weight:bold;*/
 /*    margin: 20px;*/
 /*}*/
 
</style>

<?php get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			while ( have_posts() ) :
				
				the_post();

				get_template_part( 'template-parts/page/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>
			<aside id="secondary" class="widget-area" role="complementary" aria-label="<?php esc_attr_e( 'Blog Sidebar', 'twentyseventeen' ); ?>">
				<?php dynamic_sidebar( 'sidebar-1' ); ?>
				<section id="search-2" class="widget widget_search">
				<form role="search" method="get" class="search-form" action="https://opapito.mooo.com/wordpress/">
					<label for="search-form-1">
						<span class="screen-reader-text">Search for:</span>
					</label>
					<input type="search" id="search-form-1" class="search-field" placeholder="Search &hellip;" value="" name="s" />
					<button type="submit" class="search-submit"><svg class="icon icon-search" aria-hidden="true" role="img"> <use href="#icon-search" xlink:href="#icon-search"></use> </svg><span class="screen-reader-text">Search</span></button>
				</form>
			</aside>

		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->



